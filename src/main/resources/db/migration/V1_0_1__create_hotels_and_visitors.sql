CREATE TABLE `hotels` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `lang` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ranking` int NOT NULL,
  `city_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKs4hjpubog1wqi4qktjpoejarg` (`city_id`),
  CONSTRAINT `FKs4hjpubog1wqi4qktjpoejarg` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`)
);

CREATE TABLE `visitors` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `lang` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `hotels_visitors` (
  `hotel_id` bigint NOT NULL,
  `visitor_id` bigint NOT NULL,
  KEY `FK_TO_VISITORS_TABLE` (`visitor_id`),
  KEY `FK_TO_HOTELS_TABLE` (`hotel_id`),
  CONSTRAINT `FK_TO_HOTELS_TABLE` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  CONSTRAINT `FK_TO_VISITORS_TABLE` FOREIGN KEY (`visitor_id`) REFERENCES `visitors` (`id`)
);
package org.hillel.spring.app.controller;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.hillel.spring.app.dto.CityDto;
import org.hillel.spring.app.service.CityService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cities")
@RequiredArgsConstructor
public class CityController implements CitiesApi {

  private final CityService service;

  @GetMapping
  public ResponseEntity<List<CityDto>> getAllCities() {
    return ResponseEntity.ok(service.getAll());
  }

  @GetMapping("/{id}")
  public ResponseEntity<CityDto> getCityById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(service.getById(id));
  }

  @PostMapping
  public ResponseEntity<CityDto> createCity(@RequestBody CityDto dto) {
    return ResponseEntity.ok(service.create(dto));
  }

  @PutMapping
  public ResponseEntity<CityDto> updateCity(@RequestBody CityDto dto) {
    return ResponseEntity.ok(service.update(dto));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deleteCity(@PathVariable("id") Long id) {
    service.delete(id);
    return ResponseEntity.ok().build();
  }

}

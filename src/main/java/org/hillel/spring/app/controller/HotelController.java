package org.hillel.spring.app.controller;

import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.hillel.spring.app.dto.HotelDto;
import org.hillel.spring.app.service.HotelService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hotels")
@RequiredArgsConstructor
public class HotelController implements HotelsApi {

  private final HotelService service;

  @GetMapping
  public ResponseEntity<List<HotelDto>> getAllHotels(@RequestParam("page") Integer page,
                                                     @RequestParam("size") Integer size) {
    return ResponseEntity.ok(service.getAll(page, size));
  }

  @GetMapping("/{id}")
  public ResponseEntity<HotelDto> getHotelById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(service.getById(id));
  }

  @GetMapping("/query")
  public ResponseEntity<HotelDto> getHotelByNameAndRanking(@RequestParam("name") String name,
                                                      @RequestParam("ranking") Integer ranking) {
    return ResponseEntity.ok(service.getByNameAndRanking(name, ranking));
  }

  @PostMapping
  public ResponseEntity<HotelDto> createHotel(@Valid @RequestBody HotelDto dto) {
    return ResponseEntity.ok(service.create(dto));
  }

  @PutMapping
  public ResponseEntity<HotelDto> updateHotel(@RequestBody HotelDto dto) {
    return ResponseEntity.ok(service.update(dto));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deleteHotel(@PathVariable("id") Long id) {
    service.delete(id);
    return ResponseEntity.ok().build();
  }

//  @ExceptionHandler({MethodArgumentNotValidException.class})
//  public ResponseEntity<ErrorDto> handleException(MethodArgumentNotValidException ex, HttpServletRequest request) {
//
//    String message = ex.getFieldError().getDefaultMessage();
//    String field = ex.getFieldError().getField();
//
//    return ResponseEntity.badRequest()
//        .body(ErrorProvider.getBadRequest(field + " field " + message, request.getServletPath()));
//  }

}

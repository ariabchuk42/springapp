package org.hillel.spring.app.controller;

import lombok.RequiredArgsConstructor;
import org.hillel.spring.app.dto.CredentialsDto;
import org.hillel.spring.app.dto.TokenResponseDto;
import org.hillel.spring.app.security.JwtProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController implements AuthApi {

  private final JwtProvider provider;

  @PostMapping("/token")
  public ResponseEntity<TokenResponseDto> provideToken(@RequestBody CredentialsDto dto) {
    TokenResponseDto responseDto = new TokenResponseDto();
    responseDto.setToken(provider.getToken(dto));
    responseDto.setExpiration(JwtProvider.EXPIRATION);
    responseDto.setTokenType(JwtProvider.TOKEN_TYPE);

    return ResponseEntity.ok(responseDto);
  }

}

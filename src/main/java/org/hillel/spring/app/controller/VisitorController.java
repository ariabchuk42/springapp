package org.hillel.spring.app.controller;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.hillel.spring.app.dto.VisitorDto;
import org.hillel.spring.app.service.VisitorProvider;
import org.hillel.spring.app.service.VisitorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/visitors")
@RequiredArgsConstructor
public class VisitorController implements VisitorsApi {

  private final VisitorService service;
  private final VisitorProvider provider;

  @GetMapping
  public ResponseEntity<List<VisitorDto>> getAllVisitors() {
    return ResponseEntity.ok(service.getAll());
  }

  @GetMapping("/{id}")
  public ResponseEntity<VisitorDto> getVisitorById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(service.getById(id));
  }

  @Override
  public ResponseEntity<Object> provideVisitors(Integer amount) throws Exception {
    provider.addVisitor(amount);
    return ResponseEntity.ok().build();
  }

  @PostMapping
  public ResponseEntity<VisitorDto> createVisitor(@RequestBody VisitorDto dto) {
    return ResponseEntity.ok(service.create(dto));
  }

  @PutMapping
  public ResponseEntity<VisitorDto> updateVisitor(@RequestBody VisitorDto dto) {
    return ResponseEntity.ok(service.update(dto));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deleteVisitor(@PathVariable("id") Long id) {
    service.delete(id);
    return ResponseEntity.ok().build();
  }


}

package org.hillel.spring.app.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.Key;
import java.util.Base64;
import java.util.Date;
import javax.crypto.spec.SecretKeySpec;
import org.hillel.spring.app.dto.CredentialsDto;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class JwtProvider {

  public static final Long EXPIRATION = 3600000L;
  public static final String TOKEN_TYPE = "Bearer";
  private static final String SECRET = "eibccbdvljgvciltgicrftttjgduekltdnrkhlfffcjk";

  private final Key key = new SecretKeySpec(Base64.getDecoder().decode(SECRET), SignatureAlgorithm.HS256.getJcaName());

  public String getToken(CredentialsDto dto) {

    Date now = new Date();
    Date expire = new Date(now.getTime() + EXPIRATION);

    return Jwts.builder()
        .setSubject(dto.getUsername())
        .setIssuedAt(now)
        .setExpiration(expire)
        .signWith(key)
        .compact();
  }

  public String getUser(String token) {
    return Jwts.parserBuilder()
        .setSigningKey(key)
        .build()
        .parseClaimsJws(token)
        .getBody()
        .getSubject();
  }

  public boolean validate(String token) {
    try {
      Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
      return true;
    } catch (Exception ex) {
      throw new AuthenticationCredentialsNotFoundException("JWT has expired or incorrect");
    }
  }
}

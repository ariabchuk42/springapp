package org.hillel.spring.app.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
public class UserDetailConfig {

  @Bean
  public UserDetailsService getUserDetailsService() {
    UserDetails user = User.builder()
        .username("user")
        .password(getPassEncoder().encode("pass"))
        .roles("USER")
        .build();

    UserDetails admin = User.builder()
        .username("admin")
        .password(getPassEncoder().encode("pass"))
        .roles("ADMIN")
        .build();

    return new InMemoryUserDetailsManager(user, admin);

  }

  @Bean
  public PasswordEncoder getPassEncoder() {
    return new BCryptPasswordEncoder();
  }
}

package org.hillel.spring.app.util;

import java.time.Instant;
import java.util.Date;
import org.hillel.spring.app.dto.ErrorDto;

public class ErrorProvider {

  public static ErrorDto getBadRequest(String message, String path) {
    ErrorDto errorDto = new ErrorDto();
    errorDto.setError("Bad Request");
    errorDto.setMessage(message);
    errorDto.setStatus(400);
    errorDto.setTimestamp(new Date());
    errorDto.setPath(path);
    return errorDto;
  }
}

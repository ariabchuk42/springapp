package org.hillel.spring.app.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode(of = {"name", "description", "lang", "ranking"})
@Entity
@Table(name = "hotels")
public class Hotel {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private String description;
  private String lang;
  private int ranking;

  @ManyToMany
  @JoinTable(
      name = "hotels_visitors",
      joinColumns = { @JoinColumn(name = "hotel_id") },
      inverseJoinColumns = { @JoinColumn(name = "visitor_id") }
  )
  private List<Visitor> visitors;

}

package org.hillel.spring.app.handler;

import javax.servlet.http.HttpServletRequest;
import org.hillel.spring.app.dto.ErrorDto;
import org.hillel.spring.app.util.ErrorProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BookingExceptionHandler {

  @ExceptionHandler({MethodArgumentNotValidException.class})
  public ResponseEntity<ErrorDto> handleException(MethodArgumentNotValidException ex, HttpServletRequest request) {

    String message = ex.getFieldError().getDefaultMessage();
    String field = ex.getFieldError().getField();

    return ResponseEntity.badRequest()
        .body(ErrorProvider.getBadRequest(field + " field " + message, request.getServletPath()));
  }

  @ExceptionHandler({Exception.class})
  public ResponseEntity<ErrorDto> handleGlobalException(Exception ex, HttpServletRequest request) {

    return ResponseEntity.badRequest()
        .body(ErrorProvider.getBadRequest(ex.getMessage(), request.getServletPath()));
  }
}

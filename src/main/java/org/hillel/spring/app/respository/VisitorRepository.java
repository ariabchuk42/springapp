package org.hillel.spring.app.respository;

import org.hillel.spring.app.entity.Visitor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VisitorRepository extends JpaRepository<Visitor, Long> {
}

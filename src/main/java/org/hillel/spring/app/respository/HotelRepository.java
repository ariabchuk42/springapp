package org.hillel.spring.app.respository;

import java.util.List;
import org.hillel.spring.app.entity.Hotel;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface HotelRepository extends JpaRepository<Hotel, Long> {

  List<Hotel> findByName(String name);
  List<Hotel> findByDescription(String description);

  List<Hotel> findByDescriptionAndLang(String description, String lang);

  @Query("SELECT h FROM Hotel h")
  List<Hotel> getData();

  @Query(value = "SELECT * FROM hotels h", nativeQuery = true)
  List<Hotel> getData2();

  @Query("SELECT h FROM Hotel h WHERE h.name = ?1 AND h.ranking = ?2")
  List<Hotel> getData3(String name, int ranking);

  @Query(value = "SELECT * FROM hotels h WHERE h.name = ?1 AND h.ranking = ?2", nativeQuery = true)
  List<Hotel> getData4(String name, int ranking);

  @Query("SELECT h FROM Hotel h WHERE h.name = :name AND h.ranking = :ranking")
  List<Hotel> getData5(@Param("name") String name, @Param("ranking") int ranking);

  @Query(value = "SELECT * FROM hotels h WHERE h.name = :name AND h.ranking = :ranking", nativeQuery = true)
  List<Hotel> getData6(@Param("name") String name, @Param("ranking") int ranking);

  @Query("SELECT h FROM Hotel h")
  List<Hotel> getDataSort(Sort sort);

}

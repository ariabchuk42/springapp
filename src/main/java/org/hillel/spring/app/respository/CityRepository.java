package org.hillel.spring.app.respository;

import org.hillel.spring.app.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Long> {
}

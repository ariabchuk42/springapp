package org.hillel.spring.app.service;

import java.util.List;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.hillel.spring.app.dto.CityDto;
import org.hillel.spring.app.mapper.CityMapper;
import org.hillel.spring.app.respository.CityRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
@Transactional
public class CityService {

  private final CityRepository repository;
  private final CityMapper mapper;


  public List<CityDto> getAll() {

    log.info("Getting all cities");
    return mapper.toDto(repository.findAll());
  }

  public CityDto getById(Long id) {
    log.info("Getting city by id: {}", id);
    return mapper.toDto(repository.getReferenceById(id));
  }

  public CityDto create(CityDto city) {
    city.setId(null);
    log.info("Creating city: {}", city);
    return mapper.toDto(repository.save(mapper.toEntity(city)));
  }

  public CityDto update(CityDto city) {
    log.info("Updating city: {}", city);
    return mapper.toDto(repository.save(mapper.toEntity(city)));
  }

  public void delete(Long id) {
    log.info("Deleting city by Id: {}", id);
    repository.deleteById(id);
  }

}

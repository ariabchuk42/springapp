package org.hillel.spring.app.service;

import java.util.List;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.hillel.spring.app.dto.VisitorDto;
import org.hillel.spring.app.mapper.VisitorMapper;
import org.hillel.spring.app.respository.VisitorRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
@Transactional
public class VisitorService {

  private final VisitorRepository repository;
  private final VisitorMapper mapper;

  public List<VisitorDto> getAll() {

    log.info("Getting all visitors");
    return mapper.toDto(repository.findAll());
  }

  public VisitorDto getById(Long id) {
    log.info("Getting Visitor by id: {}", id);
    return mapper.toDto(repository.getReferenceById(id));
  }

  public VisitorDto create(VisitorDto visitor) {
    visitor.setId(null);
    log.info("Creating visitor: {}", visitor);
    return mapper.toDto(repository.save(mapper.toEntity(visitor)));
  }

  public VisitorDto update(VisitorDto visitor) {
    log.info("Updating visitor: {}", visitor);
    return mapper.toDto(repository.save(mapper.toEntity(visitor)));
  }

  public void delete(Long id) {
    log.info("Deleting Visitor by Id: {}", id);
    repository.deleteById(id);
  }

}

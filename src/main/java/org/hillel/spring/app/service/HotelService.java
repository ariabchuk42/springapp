package org.hillel.spring.app.service;

import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.hillel.spring.app.dto.HotelDto;
import org.hillel.spring.app.mapper.HotelMapper;
import org.hillel.spring.app.respository.HotelRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
@Transactional
public class HotelService {

  private final HotelRepository repository;
  private final HotelMapper mapper;

  public List<HotelDto> getAll(int page, int size) {

    log.info("Getting all hotels");
    Pageable pageable = PageRequest.of(page, size);
    return repository.findAll(pageable).stream()
        .map(mapper::toDto)
        .collect(Collectors.toList());

  }

  public HotelDto getById(Long id) {
    log.info("Getting Hotel by id: {}", id);
    return mapper.toDto(repository.getReferenceById(id));
  }

  public HotelDto getByNameAndRanking(String name, int ranking) {
    log.info("Getting Hotel by name: {} and ranking: {}", name, ranking);
    return mapper.toDto(repository.getData4(name, ranking)).stream().findAny().orElse(null);
  }

  public HotelDto create(HotelDto hotel) {
    hotel.setId(null);
    log.info("Creating hotel: {}", hotel);
    return mapper.toDto(repository.save(mapper.toEntity(hotel)));
  }

  public HotelDto update(HotelDto hotel) {
    log.info("Updating hotel: {}", hotel);
    return mapper.toDto(repository.save(mapper.toEntity(hotel)));
  }

  public void delete(Long id) {
    log.info("Deleting Hotel by Id: {}", id);
    repository.deleteById(id);
  }

}

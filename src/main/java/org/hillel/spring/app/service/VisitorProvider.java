package org.hillel.spring.app.service;

import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hillel.spring.app.entity.Visitor;
import org.hillel.spring.app.respository.VisitorRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
@Log4j2
public class VisitorProvider {

  private final VisitorRepository repository;
  private final RestTemplate restTemplate;

  public void addVisitor(Integer amount) {

    ResponseEntity<ResponseDto> response =
        restTemplate.getForEntity("https://randomuser.me/api/?results=" + amount, ResponseDto.class);

    Objects.requireNonNull(response.getBody()).getResults().forEach(r -> {
      Visitor visitor = new Visitor();

      visitor.setFirstName(r.getName().getFirst());
      visitor.setLastName(r.getName().getLast());
      visitor.setLang("ENG");

      repository.save(visitor);
    });


    log.info("visitors: {}", response.getBody());

  }


  @Getter
  @Setter
  @RequiredArgsConstructor
  private static class ResponseDto {
    private List<ResultDto> results;
  }

  @Getter
  @Setter
  @RequiredArgsConstructor
  private static class ResultDto {
    private NameDto name;
  }

  @Getter
  @Setter
  @RequiredArgsConstructor
  private static class NameDto {
    private String first;
    private String last;
  }

}

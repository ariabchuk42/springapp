package org.hillel.spring.app.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StringContainsValidator implements ConstraintValidator<StringContains, String> {

  private String val;

  @Override
  public void initialize(StringContains constraintAnnotation) {
    this.val = constraintAnnotation.value();
  }

  @Override
  public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
    return s.toLowerCase().contains(val.toLowerCase());
  }
}

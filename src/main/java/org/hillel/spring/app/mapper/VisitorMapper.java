package org.hillel.spring.app.mapper;

import java.util.List;
import org.hillel.spring.app.dto.VisitorDto;
import org.hillel.spring.app.entity.Visitor;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VisitorMapper {

  Visitor toEntity(VisitorDto dto);
  VisitorDto toDto(Visitor Visitor);

  List<Visitor> toEntity(List<VisitorDto> dtos);
  List<VisitorDto> toDto(List<Visitor> entities);
}

package org.hillel.spring.app.mapper;

import java.util.List;
import org.hillel.spring.app.dto.CityDto;
import org.hillel.spring.app.entity.City;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {HotelMapper.class})
public interface CityMapper {

  City toEntity(CityDto dto);
  CityDto toDto(City entity);

  List<City> toEntity(List<CityDto> dtos);
  List<CityDto> toDto(List<City> entities);
}

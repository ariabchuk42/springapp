package org.hillel.spring.app.mapper;

import java.util.List;
import org.hillel.spring.app.dto.HotelDto;
import org.hillel.spring.app.entity.Hotel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface HotelMapper {

  Hotel toEntity(HotelDto dto);
  HotelDto toDto(Hotel entity);

  List<Hotel> toEntity(List<HotelDto> dtos);
  List<HotelDto> toDto(List<Hotel> entities);
}

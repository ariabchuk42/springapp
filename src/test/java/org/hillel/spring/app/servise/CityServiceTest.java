package org.hillel.spring.app.servise;

import java.util.List;
import org.hillel.spring.app.dto.CityDto;
import org.hillel.spring.app.service.CityService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CityServiceTest {

  @Autowired
  private CityService service;

  @Test
  void getAllCitiesTest() {

    List<CityDto> list = service.getAll();

    Assertions.assertTrue(list.size() > 1);

  }

  @Test
  void getCityByIdTest() {

    CityDto city = service.getById(1L);

    Assertions.assertTrue(city.getHotels().size() > 1);

  }
}

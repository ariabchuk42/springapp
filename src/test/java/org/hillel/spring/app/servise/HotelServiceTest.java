package org.hillel.spring.app.servise;

import java.util.List;
import org.hillel.spring.app.dto.HotelDto;
import org.hillel.spring.app.service.HotelService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class HotelServiceTest {

  @Autowired
  private HotelService service;

  @Test
  void getAllTest() {

    List<HotelDto> list = service.getAll(0, 3);

    Assertions.assertEquals(3, list.size());


  }


}

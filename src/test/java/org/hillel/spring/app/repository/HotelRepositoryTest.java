package org.hillel.spring.app.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.hillel.spring.app.entity.Hotel;
import org.hillel.spring.app.respository.HotelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

@SpringBootTest
public class HotelRepositoryTest {

  @Autowired
  private HotelRepository repository;

  @Test
  void getAllTest() {

    List<Hotel> hotels = repository.findAll();

    assertTrue(hotels.size() > 2);

  }

  @Test
  void getHotelByName() {

    List<Hotel> hotels = repository.findByName("hilton5");

    assertEquals(1, hotels.size());
    assertEquals("hilton5", hotels.get(0).getName());
    assertEquals("hilton hotel", hotels.get(0).getDescription());

  }

  @Test
  void getHotelByDescription() {

    List<Hotel> hotels = repository.findByDescription("hilton hotel");

    assertEquals(6, hotels.size());

  }

  @Test
  void getHotelByDescriptionAndLang() {

    List<Hotel> hotels = repository.findByDescriptionAndLang("hilton hotel", "ENG");

    assertEquals(2, hotels.size());

  }

  @Test
  void getAllQuery() {

    List<Hotel> list = repository.getData();
    List<Hotel> list2 = repository.getData2();

    Assertions.assertEquals(list.size(), list2.size());

  }

  @Test
  void getQueryByNameAndRanking() {

    List<Hotel> list = repository.getData3("hilton2", 3);
    List<Hotel> list2 = repository.getData4("hilton2", 3);
    List<Hotel> list3 = repository.getData5("hilton2", 3);
    List<Hotel> list4 = repository.getData6("hilton2", 3);

    Assertions.assertEquals("hilton hotel", list.get(0).getDescription());
    Assertions.assertEquals("hilton hotel", list2.get(0).getDescription());
    Assertions.assertEquals("hilton hotel", list3.get(0).getDescription());
    Assertions.assertEquals("hilton hotel", list4.get(0).getDescription());

  }

  @Test
  void getSortedTest() {

    List<Hotel> list = repository.getDataSort(Sort.by("name"));

    Assertions.assertTrue(list.size() > 5);
  }

}
